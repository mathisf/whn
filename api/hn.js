module.exports = (req, res) => {
    const name = req?.query?.name;

    if (!['top', 'best', 'show', 'ask', 'noob', 'shownew', 'active', 'jobs'].includes(name)) {
        res.status(404).send(`Page not found.`)
    }

    const url = `https://hacker-news.firebaseio.com/v0/${name}stories.json`;

    fetch(url).then((ids) => {
        res.status(200).send(ids);
    });
}
