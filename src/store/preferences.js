import { writable } from 'svelte/store';

export let autoShowReplies;
export let extendedMenu;
export let fontSize;
export let greentextQuote;
export let accentColor;
export let darkMode;

if (typeof window !== 'undefined') {
    autoShowReplies = JSON.parse(localStorage.getItem('auto-show-replies'));
    extendedMenu = JSON.parse(localStorage.getItem('extended-menu'));
    fontSize = localStorage.getItem('comment-font-size');
    greentextQuote = JSON.parse(localStorage.getItem('greentext-quote'));
    darkMode = localStorage.getItem('darkMode');
    accentColor = localStorage.getItem('accent-color');

    if (!autoShowReplies) {
        localStorage.setItem('auto-show-replies', JSON.stringify(false));
        autoShowReplies = writable(false);
    } else {
        autoShowReplies = writable(autoShowReplies);
    }

    if (!extendedMenu) {
        localStorage.setItem('extended-menu', JSON.stringify(false));
        extendedMenu = writable(false);
    } else {
        extendedMenu = writable(extendedMenu);
    }

    if (!/^[0-9]+$/g.test(fontSize)) {
        localStorage.setItem('comment-font-size', '13');
        fontSize = writable('13');
    } else {
        fontSize = writable(fontSize);
    }

    if (!greentextQuote) {
        localStorage.setItem('greentext-quote', JSON.stringify(true));
        greentextQuote = writable(true);
    } else {
        greentextQuote = writable(greentextQuote);
    }

    if (!accentColor) {
        localStorage.setItem('accent-color', '#ff6600');
        accentColor = writable('#ff6600');
    } else {
        accentColor = writable(accentColor);
    }

    if (!darkMode) {
        localStorage.setItem('dark-mode', JSON.stringify(true));
        darkMode = writable(true);
    } else {
        darkMode = writable(darkMode);
    }
} else {
    autoShowReplies = writable(false);
    extendedMenu = writable(false);
    fontSize = writable('14');
    greentextQuote = writable(true);
    accentColor = writable('#ff6600');
    darkMode = writable(true);
}
